﻿using UnityEngine;
using System.Collections;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using System;
using UnityEngine.UI;
//using Assets.Scripts.Types;



public class ValueBox : MonoBehaviour {

    ////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////// New Game /////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    static int Year = 57, Month = 6, Day = 13;

    static int startGold = 100000;
    static int startGPS = 2;

    static int startMana = 0;
    static int startMPS = 2;

    static int startMages = 2000;
    static int startMagesCostModifier = 100;
    
    public static float UpdateTime = 0.5f;


    ////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////// Info /////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    public static DateTime GameFirstStartDate;
    public static DateTime GameStartDate;

    ////////////////////////////////////////////////////////////////////////////
    //////////////////////////////// Game Data /////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    public static VerbianDate date = new VerbianDate(Year, Month, Day);
    public static Mages mages = new Mages(1, startMages, startMagesCostModifier, 0, 0, 0);
    public static Gold gold = new Gold(startGold, startGPS);
    public static Mana mana = new Mana(startMana, startMPS);
    public static Spells spells=new Spells(new Spell(1,"Fire",1),new Spell(1,"Fire",1),new Spell(1,"Earth",1),new Spell(1,"Wind",1));
    public static Cities cities = new Cities(new City(0, 0, 0, 0, 0, 1, false, 1, 1, 1), new City(0, 0, 0, 0, 0, 1, false, 1, 1, 1), new City(0, 0, 0, 0, 0, 1, false, 1, 1, 1), new City(0, 0, 0, 0, 0, 1, false, 1, 1, 1));
    public static Upgrades upgrades = new Upgrades(new Upgrade(1, 1), new Upgrade(1, 1), new Upgrade(1, 1));
    public static Constructions constructions=new Constructions(new Building(1,1,1));
    public static Emperor emperor = new Emperor("Carl", "the Idiot", new List<ModifierSkill>(), new VerbianDate(50, 3, 2), new VerbianDate(250, 3, 2), 200, 0, 0, 0, 1);

    ////////////////////////////////////////////////////////////////////////////
    //////////////////////////////// References ////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    public static Slider sliderMagesRecruit,
                         sliderMagesGold,
                         sliderMagesMana,
                         sliderMagesWar;

    

	public enum Element{
		Wind, Fire, Water, Stone, Metal
	}



    float timer = 0;
	void Update()
	{
        gold.Update();
        mana.Update();
        mages.Update();
        date.Update();

		timer += Time.deltaTime;
        if (timer > UpdateTime) 
		{
            gold.Gather();
            mana.Gather();
			timer = 0;
		}
	}

    
    
    void Awake()
    {
        sliderMagesRecruit = GameObject.FindGameObjectWithTag("SliderMagesRecruit").GetComponent<Slider>();
        sliderMagesGold = GameObject.FindGameObjectWithTag("SliderMagesToGold").GetComponent<Slider>();
        sliderMagesMana = GameObject.FindGameObjectWithTag("SliderMagesToMana").GetComponent<Slider>();
        sliderMagesWar = GameObject.FindGameObjectWithTag("SliderMagesToWar").GetComponent<Slider>();
    }
    
    
    

}
