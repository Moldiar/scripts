﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

    public class SaveGame
    {
        public static string Save()
        {
            //sgDate: !Day!Month!Year!
            string sgDate = String.Format("!" + ValueBox.date.Day.ToString() + "!" + ValueBox.date.Month.ToString() + "!" + ValueBox.date.Year.ToString() + "!");
            //sgMages: !Amount!Idle!inGold!inMana!inWar!Modifier!Power!
            string sgMages = String.Format("!" + ValueBox.mages.Amount.ToString() + "!" + ValueBox.mages.Idle.ToString() + "!" + ValueBox.mages.inGold.ToString() + "!" + ValueBox.mages.inMana.ToString() + "!" + ValueBox.mages.inWar.ToString() + "!" + ValueBox.mages.Modifier.ToString() + "!" + ValueBox.mages.Power.ToString()+"!");
            
            //sgGold: !Amount!GPS!
            string sgGold = String.Format("!"+ValueBox.gold.Amount.ToString()+"!"+ValueBox.gold.GPS.ToString()+"!");
            //sgMana: !Amount!MPS!
            string sgMana = String.Format("!" + ValueBox.mana.Amount.ToString() + "!" + ValueBox.mana.MPS.ToString() + "!");

            //sgSpellsFB: !Dmg!Level!Cooldown!Type!
            string sgSpellsFB = String.Format("!" + ValueBox.spells.Fireball.Dmg.ToString() + "!" + ValueBox.spells.Fireball.Level.ToString() + "!" + ValueBox.spells.Fireball.Cooldown.ToString() + "!" + ValueBox.spells.Fireball.Type + "!");
            //sgSpellsFBR: 
            string sgSpellsFBR = String.Format("!" + ValueBox.spells.FireballRain.Dmg.ToString() + "!" + ValueBox.spells.FireballRain.Level.ToString() + "!" + ValueBox.spells.FireballRain.Cooldown.ToString() + "!" + ValueBox.spells.FireballRain.Type + "!");
            //sgSpellsEQ: 
            string sgSpellsEQ = String.Format("!" + ValueBox.spells.Earthquake.Dmg.ToString() + "!" + ValueBox.spells.Earthquake.Level.ToString() + "!" + ValueBox.spells.Earthquake.Cooldown.ToString() + "!" + ValueBox.spells.Earthquake.Type + "!");
            //sgSpellsTh: 
            string sgSpellsTh = String.Format("!" + ValueBox.spells.Thunder.Dmg.ToString() + "!" + ValueBox.spells.Thunder.Level.ToString() + "!" + ValueBox.spells.Thunder.Cooldown.ToString() + "!" + ValueBox.spells.Thunder.Type + "!");

            string sgSpells = String.Format("#" + sgSpellsFB + "#" + sgSpellsFBR + "#" + sgSpellsEQ + "#" + sgSpellsTh + "#");

            //SingleCity: !ArmyPower!CityGPS!Conquered!EarthAffinity!FireAffinity!GoldMineLevel!MagesTurned!MetalAffinity!WaterAffinity!WindAffinity!
            //sgCities: #IlluraniaFirst#IlluraniaLast#Citadel#Omno#
            string sgCities = String.Format("#"+ValueBox.cities.IlluraniaFirst.ToString()+"#"+ValueBox.cities.IlluraniaLast.ToString()+"#"+ValueBox.cities.Citadel.ToString()+"#"+ValueBox.cities.Omno.ToString()+"#");

            //SingleUpgrade: !Level!Cost!
            //sgUpgrades: #EmperorUpgrade#MageUpgrade#ManaUpgrade#
            string sgUpgrades = String.Format("#" + ValueBox.upgrades.EmperorUpgrade.ToString() + "#" + ValueBox.upgrades.MageUpgrade.ToString() + "#" + ValueBox.upgrades.ManaUpgrade.ToString() + "#");
            
            //SingleBuilding: !Level!Cost!Value!
            //sgConstruction: #ManaPlant#
            string sgConstruction = String.Format("#" + ValueBox.constructions.ManaPlant.ToString() + "#");

            string sgEmperor = ValueBox.emperor.ToString();

            string x = String.Format("*^*"+sgDate+"*^*"+sgMages+"*^*"+sgGold+"*^*"+sgMana+"*^*"+sgSpells+"*^*"+sgCities+"*^*"+sgUpgrades+"*^*"+sgConstruction+"*^*"+sgEmperor+"*^*");

            return x;
        }
    }

