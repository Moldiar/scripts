﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class Upgrade
{
    public int Level { get; set; }
    public int Cost { get; set; }

    public Upgrade(int lvl, int cost)
    {
        this.Level = lvl;
        this.Cost = cost;
    }

    public string ToString()
    {
        return String.Format("!"+this.Level.ToString()+"!"+this.Cost.ToString()+"!");
    }
}

