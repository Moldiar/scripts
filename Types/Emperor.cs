﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class Emperor
{
    public string Name { get; set; }
    public string Title { get; set; }
    public List<ModifierSkill> ModSkills { get; set; }
    public VerbianDate DOB { get; set; }
    public VerbianDate DOD { get; set; }
    public int LifeExpectancy { get; set; }
    public int AmassedGold { get; set; }
    public int AmassedMana { get; set; }
    public int RecruitedMages { get; set; }
    public int Portrait { get; set; }
    public List<string> ConqueredCities { get; set; }

    public Emperor(string name, string title, List<ModifierSkill> modSkills, VerbianDate doB, VerbianDate doD, int lifeExpectancy, int amassedGold, int amassedMana, int recruitedMages, int portrait)
    {
        this.Name = name;
        this.Title = title;
        this.ModSkills = modSkills;
        this.DOB = doB;
        this.DOD = doD;
        this.LifeExpectancy = lifeExpectancy;
        this.AmassedGold = amassedGold;
        this.AmassedMana = amassedMana;
        this.RecruitedMages = recruitedMages;
        this.Portrait = portrait;
        this.ConqueredCities = new List<string>();
    }

    public string ToString()
    {
        string modS = "$";
        foreach (ModifierSkill ms in this.ModSkills)
        {
            modS += ms.Value.ToString()+"$";
        }
        string conC="$";
        foreach (string y in ConqueredCities)
        {
            conC += y+"$";
        }

        string dob = String.Format(this.DOB.Day.ToString()+"!"+this.DOB.Month.ToString()+"!"+this.DOB.Year.ToString());
        string dod = String.Format(this.DOD.Day.ToString() + "!" + this.DOD.Month.ToString() + "!" + this.DOD.Year.ToString());

        string x = String.Format("!"+this.Name+"!"+this.Title+"!"+modS+"!"+dob+"!"+dod+"!"+this.LifeExpectancy.ToString()+"!"+this.AmassedGold.ToString()+"!"+this.AmassedMana.ToString()+"!"+this.RecruitedMages.ToString()+"!"+this.Portrait.ToString()+"!"+conC);
        return x;
    }
}

